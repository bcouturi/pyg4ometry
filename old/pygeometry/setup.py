import os
from distutils.core import setup

#https://docs.python.org/2/distutils/examples.html

installRequisites = [
    'matplotlib'
    ]

setup(name = 'pygeometry',
      version = 1
      install_requires = installRequisites
