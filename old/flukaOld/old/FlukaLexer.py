# Generated from FlukaLexer.g4 by ANTLR 4.7
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2")
        buf.write(u"\31\u0119\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6")
        buf.write(u"\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4")
        buf.write(u"\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t")
        buf.write(u"\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27")
        buf.write(u"\4\30\t\30\4\31\t\31\3\2\3\2\3\2\3\2\3\3\3\3\7\3:\n\3")
        buf.write(u"\f\3\16\3=\13\3\3\3\3\3\3\4\3\4\3\4\7\4D\n\4\f\4\16\4")
        buf.write(u"G\13\4\3\4\3\4\3\5\5\5L\n\5\3\5\3\5\3\5\3\5\3\6\3\6\3")
        buf.write(u"\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3")
        buf.write(u"\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\6\tj\n\t\r\t\16\t")
        buf.write(u"k\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n")
        buf.write(u"\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3")
        buf.write(u"\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13")
        buf.write(u"\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f")
        buf.write(u"\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r")
        buf.write(u"\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16")
        buf.write(u"\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3")
        buf.write(u"\16\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17\3\17")
        buf.write(u"\3\17\3\17\3\17\3\17\3\17\3\17\3\20\5\20\u00d3\n\20\3")
        buf.write(u"\20\6\20\u00d6\n\20\r\20\16\20\u00d7\3\21\3\21\5\21\u00dc")
        buf.write(u"\n\21\5\21\u00de\n\21\3\21\6\21\u00e1\n\21\r\21\16\21")
        buf.write(u"\u00e2\3\21\5\21\u00e6\n\21\3\21\7\21\u00e9\n\21\f\21")
        buf.write(u"\16\21\u00ec\13\21\3\21\5\21\u00ef\n\21\3\21\5\21\u00f2")
        buf.write(u"\n\21\3\21\7\21\u00f5\n\21\f\21\16\21\u00f8\13\21\3\21")
        buf.write(u"\3\21\6\21\u00fc\n\21\r\21\16\21\u00fd\5\21\u0100\n\21")
        buf.write(u"\3\22\3\22\3\23\3\23\3\23\7\23\u0107\n\23\f\23\16\23")
        buf.write(u"\u010a\13\23\3\24\3\24\3\24\3\24\3\25\3\25\3\26\3\26")
        buf.write(u"\3\27\3\27\3\30\3\30\3\31\3\31\2\2\32\3\3\5\4\7\5\t\6")
        buf.write(u"\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20")
        buf.write(u"\37\21!\22#\2%\23\'\24)\25+\26-\27/\30\61\31\3\2\16\4")
        buf.write(u"\2\13\13\"\"\4\2\f\f\17\17\4\2%%,,\3\2C\\\4\2C\\c|\6")
        buf.write(u"\2\62;C\\aac|\4\2UUuu\4\2GGgg\4\2--//\3\2\62;\7\2//\62")
        buf.write(u";C\\aac|\5\2..\61\61<=\2\u0128\2\3\3\2\2\2\2\5\3\2\2")
        buf.write(u"\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2")
        buf.write(u"\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2")
        buf.write(u"\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2")
        buf.write(u"\37\3\2\2\2\2!\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2")
        buf.write(u"\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\3")
        buf.write(u"\63\3\2\2\2\5\67\3\2\2\2\7@\3\2\2\2\tK\3\2\2\2\13Q\3")
        buf.write(u"\2\2\2\rX\3\2\2\2\17]\3\2\2\2\21f\3\2\2\2\23m\3\2\2\2")
        buf.write(u"\25\177\3\2\2\2\27\u0090\3\2\2\2\31\u00a2\3\2\2\2\33")
        buf.write(u"\u00b2\3\2\2\2\35\u00c1\3\2\2\2\37\u00d2\3\2\2\2!\u00dd")
        buf.write(u"\3\2\2\2#\u0101\3\2\2\2%\u0103\3\2\2\2\'\u010b\3\2\2")
        buf.write(u"\2)\u010f\3\2\2\2+\u0111\3\2\2\2-\u0113\3\2\2\2/\u0115")
        buf.write(u"\3\2\2\2\61\u0117\3\2\2\2\63\64\t\2\2\2\64\65\3\2\2\2")
        buf.write(u"\65\66\b\2\2\2\66\4\3\2\2\2\67;\7#\2\28:\n\3\2\298\3")
        buf.write(u"\2\2\2:=\3\2\2\2;9\3\2\2\2;<\3\2\2\2<>\3\2\2\2=;\3\2")
        buf.write(u"\2\2>?\b\3\2\2?\6\3\2\2\2@A\t\4\2\2AE\6\4\2\2BD\n\3\2")
        buf.write(u"\2CB\3\2\2\2DG\3\2\2\2EC\3\2\2\2EF\3\2\2\2FH\3\2\2\2")
        buf.write(u"GE\3\2\2\2HI\b\4\2\2I\b\3\2\2\2JL\7\17\2\2KJ\3\2\2\2")
        buf.write(u"KL\3\2\2\2LM\3\2\2\2MN\7\f\2\2NO\3\2\2\2OP\b\5\3\2P\n")
        buf.write(u"\3\2\2\2QR\7G\2\2RS\6\6\3\2ST\7P\2\2TU\7F\2\2UV\3\2\2")
        buf.write(u"\2VW\b\6\2\2W\f\3\2\2\2XY\t\5\2\2YZ\6\7\4\2Z[\t\5\2\2")
        buf.write(u"[\\\t\5\2\2\\\16\3\2\2\2]^\7N\2\2^_\6\b\5\2_`\7C\2\2")
        buf.write(u"`a\7V\2\2ab\7V\2\2bc\7K\2\2cd\7E\2\2de\7G\2\2e\20\3\2")
        buf.write(u"\2\2fg\t\6\2\2gi\6\t\6\2hj\t\7\2\2ih\3\2\2\2jk\3\2\2")
        buf.write(u"\2ki\3\2\2\2kl\3\2\2\2l\22\3\2\2\2mn\7&\2\2no\6\n\7\2")
        buf.write(u"op\t\b\2\2pq\7v\2\2qr\7c\2\2rs\7t\2\2st\7v\2\2tu\7a\2")
        buf.write(u"\2uv\7g\2\2vw\7z\2\2wx\7r\2\2xy\7c\2\2yz\7p\2\2z{\7u")
        buf.write(u"\2\2{|\7k\2\2|}\7q\2\2}~\7p\2\2~\24\3\2\2\2\177\u0080")
        buf.write(u"\7&\2\2\u0080\u0081\6\13\b\2\u0081\u0082\t\b\2\2\u0082")
        buf.write(u"\u0083\7v\2\2\u0083\u0084\7c\2\2\u0084\u0085\7t\2\2\u0085")
        buf.write(u"\u0086\7v\2\2\u0086\u0087\7a\2\2\u0087\u0088\7v\2\2\u0088")
        buf.write(u"\u0089\7t\2\2\u0089\u008a\7c\2\2\u008a\u008b\7p\2\2\u008b")
        buf.write(u"\u008c\7u\2\2\u008c\u008d\7n\2\2\u008d\u008e\7c\2\2\u008e")
        buf.write(u"\u008f\7v\2\2\u008f\26\3\2\2\2\u0090\u0091\7&\2\2\u0091")
        buf.write(u"\u0092\6\f\t\2\u0092\u0093\t\b\2\2\u0093\u0094\7v\2\2")
        buf.write(u"\u0094\u0095\7c\2\2\u0095\u0096\7t\2\2\u0096\u0097\7")
        buf.write(u"v\2\2\u0097\u0098\7a\2\2\u0098\u0099\7v\2\2\u0099\u009a")
        buf.write(u"\7t\2\2\u009a\u009b\7c\2\2\u009b\u009c\7p\2\2\u009c\u009d")
        buf.write(u"\7u\2\2\u009d\u009e\7h\2\2\u009e\u009f\7q\2\2\u009f\u00a0")
        buf.write(u"\7t\2\2\u00a0\u00a1\7o\2\2\u00a1\30\3\2\2\2\u00a2\u00a3")
        buf.write(u"\7&\2\2\u00a3\u00a4\6\r\n\2\u00a4\u00a5\t\t\2\2\u00a5")
        buf.write(u"\u00a6\7p\2\2\u00a6\u00a7\7f\2\2\u00a7\u00a8\7a\2\2\u00a8")
        buf.write(u"\u00a9\7g\2\2\u00a9\u00aa\7z\2\2\u00aa\u00ab\7r\2\2\u00ab")
        buf.write(u"\u00ac\7c\2\2\u00ac\u00ad\7p\2\2\u00ad\u00ae\7u\2\2\u00ae")
        buf.write(u"\u00af\7k\2\2\u00af\u00b0\7q\2\2\u00b0\u00b1\7p\2\2\u00b1")
        buf.write(u"\32\3\2\2\2\u00b2\u00b3\7&\2\2\u00b3\u00b4\6\16\13\2")
        buf.write(u"\u00b4\u00b5\t\t\2\2\u00b5\u00b6\7p\2\2\u00b6\u00b7\7")
        buf.write(u"f\2\2\u00b7\u00b8\7a\2\2\u00b8\u00b9\7v\2\2\u00b9\u00ba")
        buf.write(u"\7t\2\2\u00ba\u00bb\7c\2\2\u00bb\u00bc\7p\2\2\u00bc\u00bd")
        buf.write(u"\7u\2\2\u00bd\u00be\7n\2\2\u00be\u00bf\7c\2\2\u00bf\u00c0")
        buf.write(u"\7v\2\2\u00c0\34\3\2\2\2\u00c1\u00c2\7&\2\2\u00c2\u00c3")
        buf.write(u"\6\17\f\2\u00c3\u00c4\t\t\2\2\u00c4\u00c5\7p\2\2\u00c5")
        buf.write(u"\u00c6\7f\2\2\u00c6\u00c7\7a\2\2\u00c7\u00c8\7v\2\2\u00c8")
        buf.write(u"\u00c9\7t\2\2\u00c9\u00ca\7c\2\2\u00ca\u00cb\7p\2\2\u00cb")
        buf.write(u"\u00cc\7u\2\2\u00cc\u00cd\7h\2\2\u00cd\u00ce\7q\2\2\u00ce")
        buf.write(u"\u00cf\7t\2\2\u00cf\u00d0\7o\2\2\u00d0\36\3\2\2\2\u00d1")
        buf.write(u"\u00d3\7/\2\2\u00d2\u00d1\3\2\2\2\u00d2\u00d3\3\2\2\2")
        buf.write(u"\u00d3\u00d5\3\2\2\2\u00d4\u00d6\5#\22\2\u00d5\u00d4")
        buf.write(u"\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7\u00d5\3\2\2\2\u00d7")
        buf.write(u"\u00d8\3\2\2\2\u00d8 \3\2\2\2\u00d9\u00de\7-\2\2\u00da")
        buf.write(u"\u00dc\7/\2\2\u00db\u00da\3\2\2\2\u00db\u00dc\3\2\2\2")
        buf.write(u"\u00dc\u00de\3\2\2\2\u00dd\u00d9\3\2\2\2\u00dd\u00db")
        buf.write(u"\3\2\2\2\u00de\u00ff\3\2\2\2\u00df\u00e1\5#\22\2\u00e0")
        buf.write(u"\u00df\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2\u00e0\3\2\2")
        buf.write(u"\2\u00e2\u00e3\3\2\2\2\u00e3\u00e5\3\2\2\2\u00e4\u00e6")
        buf.write(u"\7\60\2\2\u00e5\u00e4\3\2\2\2\u00e5\u00e6\3\2\2\2\u00e6")
        buf.write(u"\u00ea\3\2\2\2\u00e7\u00e9\5#\22\2\u00e8\u00e7\3\2\2")
        buf.write(u"\2\u00e9\u00ec\3\2\2\2\u00ea\u00e8\3\2\2\2\u00ea\u00eb")
        buf.write(u"\3\2\2\2\u00eb\u00ee\3\2\2\2\u00ec\u00ea\3\2\2\2\u00ed")
        buf.write(u"\u00ef\7G\2\2\u00ee\u00ed\3\2\2\2\u00ee\u00ef\3\2\2\2")
        buf.write(u"\u00ef\u00f1\3\2\2\2\u00f0\u00f2\t\n\2\2\u00f1\u00f0")
        buf.write(u"\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u00f6\3\2\2\2\u00f3")
        buf.write(u"\u00f5\5#\22\2\u00f4\u00f3\3\2\2\2\u00f5\u00f8\3\2\2")
        buf.write(u"\2\u00f6\u00f4\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u0100")
        buf.write(u"\3\2\2\2\u00f8\u00f6\3\2\2\2\u00f9\u00fb\7\60\2\2\u00fa")
        buf.write(u"\u00fc\5#\22\2\u00fb\u00fa\3\2\2\2\u00fc\u00fd\3\2\2")
        buf.write(u"\2\u00fd\u00fb\3\2\2\2\u00fd\u00fe\3\2\2\2\u00fe\u0100")
        buf.write(u"\3\2\2\2\u00ff\u00e0\3\2\2\2\u00ff\u00f9\3\2\2\2\u0100")
        buf.write(u"\"\3\2\2\2\u0101\u0102\t\13\2\2\u0102$\3\2\2\2\u0103")
        buf.write(u"\u0104\t\6\2\2\u0104\u0108\6\23\r\2\u0105\u0107\t\f\2")
        buf.write(u"\2\u0106\u0105\3\2\2\2\u0107\u010a\3\2\2\2\u0108\u0106")
        buf.write(u"\3\2\2\2\u0108\u0109\3\2\2\2\u0109&\3\2\2\2\u010a\u0108")
        buf.write(u"\3\2\2\2\u010b\u010c\t\r\2\2\u010c\u010d\3\2\2\2\u010d")
        buf.write(u"\u010e\b\24\2\2\u010e(\3\2\2\2\u010f\u0110\7-\2\2\u0110")
        buf.write(u"*\3\2\2\2\u0111\u0112\7/\2\2\u0112,\3\2\2\2\u0113\u0114")
        buf.write(u"\7~\2\2\u0114.\3\2\2\2\u0115\u0116\7*\2\2\u0116\60\3")
        buf.write(u"\2\2\2\u0117\u0118\7+\2\2\u0118\62\3\2\2\2\24\2;EKk\u00d2")
        buf.write(u"\u00d7\u00db\u00dd\u00e2\u00e5\u00ea\u00ee\u00f1\u00f6")
        buf.write(u"\u00fd\u00ff\u0108\4\b\2\2\2\3\2")
        return buf.getvalue()


class FlukaLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    Whitespace = 1
    InLineComment = 2
    LineComment = 3
    Newline = 4
    End = 5
    BodyCode = 6
    Lattice = 7
    RegionName = 8
    StartExpansion = 9
    StartTranslat = 10
    StartTransform = 11
    EndExpansion = 12
    EndTranslat = 13
    EndTransform = 14
    Integer = 15
    Float = 16
    ID = 17
    Delim = 18
    Plus = 19
    Minus = 20
    Bar = 21
    LParen = 22
    RParen = 23

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'+'", u"'-'", u"'|'", u"'('", u"')'" ]

    symbolicNames = [ u"<INVALID>",
            u"Whitespace", u"InLineComment", u"LineComment", u"Newline", 
            u"End", u"BodyCode", u"Lattice", u"RegionName", u"StartExpansion", 
            u"StartTranslat", u"StartTransform", u"EndExpansion", u"EndTranslat", 
            u"EndTransform", u"Integer", u"Float", u"ID", u"Delim", u"Plus", 
            u"Minus", u"Bar", u"LParen", u"RParen" ]

    ruleNames = [ u"Whitespace", u"InLineComment", u"LineComment", u"Newline", 
                  u"End", u"BodyCode", u"Lattice", u"RegionName", u"StartExpansion", 
                  u"StartTranslat", u"StartTransform", u"EndExpansion", 
                  u"EndTranslat", u"EndTransform", u"Integer", u"Float", 
                  u"Digit", u"ID", u"Delim", u"Plus", u"Minus", u"Bar", 
                  u"LParen", u"RParen" ]

    grammarFileName = u"FlukaLexer.g4"

    def __init__(self, input=None, output=sys.stdout):
        super(FlukaLexer, self).__init__(input, output=output)
        self.checkVersion("4.7")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def sempred(self, localctx, ruleIndex, predIndex):
        if self._predicates is None:
            preds = dict()
            preds[2] = self.LineComment_sempred
            preds[4] = self.End_sempred
            preds[5] = self.BodyCode_sempred
            preds[6] = self.Lattice_sempred
            preds[7] = self.RegionName_sempred
            preds[8] = self.StartExpansion_sempred
            preds[9] = self.StartTranslat_sempred
            preds[10] = self.StartTransform_sempred
            preds[11] = self.EndExpansion_sempred
            preds[12] = self.EndTranslat_sempred
            preds[13] = self.EndTransform_sempred
            preds[17] = self.ID_sempred
            self._predicates = preds
        pred = self._predicates.get(ruleIndex, None)
        if pred is not None:
            return pred(localctx, predIndex)
        else:
            raise Exception("No registered predicate for:" + str(ruleIndex))

    def LineComment_sempred(self, localctx, predIndex):
            if predIndex == 0:
                return self.column == 1
         

    def End_sempred(self, localctx, predIndex):
            if predIndex == 1:
                return self.column == 1
         

    def BodyCode_sempred(self, localctx, predIndex):
            if predIndex == 2:
                return self.column == 1
         

    def Lattice_sempred(self, localctx, predIndex):
            if predIndex == 3:
                return self.column == 1
         

    def RegionName_sempred(self, localctx, predIndex):
            if predIndex == 4:
                return self.column == 1
         

    def StartExpansion_sempred(self, localctx, predIndex):
            if predIndex == 5:
                return self.column == 1
         

    def StartTranslat_sempred(self, localctx, predIndex):
            if predIndex == 6:
                return self.column == 1
         

    def StartTransform_sempred(self, localctx, predIndex):
            if predIndex == 7:
                return self.column == 1
         

    def EndExpansion_sempred(self, localctx, predIndex):
            if predIndex == 8:
                return self.column == 1
         

    def EndTranslat_sempred(self, localctx, predIndex):
            if predIndex == 9:
                return self.column == 1
         

    def EndTransform_sempred(self, localctx, predIndex):
            if predIndex == 10:
                return self.column == 1
         

    def ID_sempred(self, localctx, predIndex):
            if predIndex == 11:
                return self.column != 1
         


