pyfluka Documentation
====================

With this package you can load FLUKA geometry view it in VTK and convert it to GDML (Geant4's own XML geometry language).  You can also do all this for a subset of the geometry's regions as well as for individual subzones thereof.

Many features are yet to be implemented or are missing.

.. toctree::
   :maxdepth: 2

   licence
   authorship
   installation
   moduledocs
   issues


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
