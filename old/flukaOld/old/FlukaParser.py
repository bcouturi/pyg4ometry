# Generated from FlukaParser.g4 by ANTLR 4.7
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3")
        buf.write(u"\31\u0096\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write(u"\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t")
        buf.write(u"\r\4\16\t\16\3\2\3\2\3\2\6\2 \n\2\r\2\16\2!\3\3\3\3\3")
        buf.write(u"\3\3\3\6\3(\n\3\r\3\16\3)\3\3\3\3\3\3\5\3/\n\3\7\3\61")
        buf.write(u"\n\3\f\3\16\3\64\13\3\5\3\66\n\3\3\4\3\4\3\4\3\4\3\4")
        buf.write(u"\3\4\5\4>\n\4\3\5\3\5\3\5\3\5\6\5D\n\5\r\5\16\5E\3\5")
        buf.write(u"\3\5\3\5\3\5\3\5\3\5\3\5\7\5O\n\5\f\5\16\5R\13\5\3\5")
        buf.write(u"\3\5\5\5V\n\5\3\6\3\6\5\6Z\n\6\3\7\3\7\3\7\3\7\3\7\3")
        buf.write(u"\7\3\7\3\7\5\7d\n\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3")
        buf.write(u"\n\3\n\3\n\5\nq\n\n\3\13\3\13\3\13\6\13v\n\13\r\13\16")
        buf.write(u"\13w\3\13\3\13\3\f\3\f\3\f\3\f\3\f\6\f\u0081\n\f\r\f")
        buf.write(u"\16\f\u0082\3\f\3\f\3\r\3\r\3\r\6\r\u008a\n\r\r\r\16")
        buf.write(u"\r\u008b\3\r\3\r\3\16\3\16\6\16\u0092\n\16\r\16\16\16")
        buf.write(u"\u0093\3\16\2\2\17\2\4\6\b\n\f\16\20\22\24\26\30\32\2")
        buf.write(u"\5\4\2\21\21\23\23\3\2\22\23\3\2\25\26\2\u009f\2\37\3")
        buf.write(u"\2\2\2\4\65\3\2\2\2\6=\3\2\2\2\bU\3\2\2\2\nY\3\2\2\2")
        buf.write(u"\fc\3\2\2\2\16e\3\2\2\2\20j\3\2\2\2\22p\3\2\2\2\24r\3")
        buf.write(u"\2\2\2\26{\3\2\2\2\30\u0086\3\2\2\2\32\u008f\3\2\2\2")
        buf.write(u"\34 \5\4\3\2\35 \5\6\4\2\36 \5\32\16\2\37\34\3\2\2\2")
        buf.write(u"\37\35\3\2\2\2\37\36\3\2\2\2 !\3\2\2\2!\37\3\2\2\2!\"")
        buf.write(u"\3\2\2\2\"\3\3\2\2\2#\66\5\22\n\2$%\7\b\2\2%\'\t\2\2")
        buf.write(u"\2&(\7\22\2\2\'&\3\2\2\2()\3\2\2\2)\'\3\2\2\2)*\3\2\2")
        buf.write(u"\2*\66\3\2\2\2+\62\7\b\2\2,.\7\24\2\2-/\t\3\2\2.-\3\2")
        buf.write(u"\2\2./\3\2\2\2/\61\3\2\2\2\60,\3\2\2\2\61\64\3\2\2\2")
        buf.write(u"\62\60\3\2\2\2\62\63\3\2\2\2\63\66\3\2\2\2\64\62\3\2")
        buf.write(u"\2\2\65#\3\2\2\2\65$\3\2\2\2\65+\3\2\2\2\66\5\3\2\2\2")
        buf.write(u"\678\7\n\2\289\7\21\2\29>\5\n\6\2:;\7\n\2\2;<\7\21\2")
        buf.write(u"\2<>\5\b\5\2=\67\3\2\2\2=:\3\2\2\2>\7\3\2\2\2?@\7\27")
        buf.write(u"\2\2@C\5\n\6\2AB\7\27\2\2BD\5\n\6\2CA\3\2\2\2DE\3\2\2")
        buf.write(u"\2EC\3\2\2\2EF\3\2\2\2FV\3\2\2\2GH\7\27\2\2HV\5\n\6\2")
        buf.write(u"IJ\5\n\6\2JP\7\27\2\2KL\5\n\6\2LM\7\27\2\2MO\3\2\2\2")
        buf.write(u"NK\3\2\2\2OR\3\2\2\2PN\3\2\2\2PQ\3\2\2\2QS\3\2\2\2RP")
        buf.write(u"\3\2\2\2ST\5\n\6\2TV\3\2\2\2U?\3\2\2\2UG\3\2\2\2UI\3")
        buf.write(u"\2\2\2V\t\3\2\2\2WZ\5\f\7\2XZ\5\16\b\2YW\3\2\2\2YX\3")
        buf.write(u"\2\2\2Z\13\3\2\2\2[d\5\20\t\2\\]\5\20\t\2]^\5\f\7\2^")
        buf.write(u"d\3\2\2\2_`\5\16\b\2`a\5\f\7\2ad\3\2\2\2bd\5\16\b\2c")
        buf.write(u"[\3\2\2\2c\\\3\2\2\2c_\3\2\2\2cb\3\2\2\2d\r\3\2\2\2e")
        buf.write(u"f\t\4\2\2fg\7\30\2\2gh\5\f\7\2hi\7\31\2\2i\17\3\2\2\2")
        buf.write(u"jk\t\4\2\2kl\7\23\2\2l\21\3\2\2\2mq\5\24\13\2nq\5\26")
        buf.write(u"\f\2oq\5\30\r\2pm\3\2\2\2pn\3\2\2\2po\3\2\2\2q\23\3\2")
        buf.write(u"\2\2rs\7\13\2\2su\7\22\2\2tv\5\4\3\2ut\3\2\2\2vw\3\2")
        buf.write(u"\2\2wu\3\2\2\2wx\3\2\2\2xy\3\2\2\2yz\7\16\2\2z\25\3\2")
        buf.write(u"\2\2{|\7\f\2\2|}\7\22\2\2}~\7\22\2\2~\u0080\7\22\2\2")
        buf.write(u"\177\u0081\5\4\3\2\u0080\177\3\2\2\2\u0081\u0082\3\2")
        buf.write(u"\2\2\u0082\u0080\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0084")
        buf.write(u"\3\2\2\2\u0084\u0085\7\17\2\2\u0085\27\3\2\2\2\u0086")
        buf.write(u"\u0087\7\r\2\2\u0087\u0089\t\2\2\2\u0088\u008a\5\4\3")
        buf.write(u"\2\u0089\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u0089")
        buf.write(u"\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008d\3\2\2\2\u008d")
        buf.write(u"\u008e\7\20\2\2\u008e\31\3\2\2\2\u008f\u0091\7\t\2\2")
        buf.write(u"\u0090\u0092\7\23\2\2\u0091\u0090\3\2\2\2\u0092\u0093")
        buf.write(u"\3\2\2\2\u0093\u0091\3\2\2\2\u0093\u0094\3\2\2\2\u0094")
        buf.write(u"\33\3\2\2\2\23\37!).\62\65=EPUYcpw\u0082\u008b\u0093")
        return buf.getvalue()


class FlukaParser ( Parser ):

    grammarFileName = "FlukaParser.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                     u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                     u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                     u"<INVALID>", u"<INVALID>", u"<INVALID>", u"<INVALID>", 
                     u"<INVALID>", u"<INVALID>", u"<INVALID>", u"'+'", u"'-'", 
                     u"'|'", u"'('", u"')'" ]

    symbolicNames = [ u"<INVALID>", u"Whitespace", u"InLineComment", u"LineComment", 
                      u"Newline", u"End", u"BodyCode", u"Lattice", u"RegionName", 
                      u"StartExpansion", u"StartTranslat", u"StartTransform", 
                      u"EndExpansion", u"EndTranslat", u"EndTransform", 
                      u"Integer", u"Float", u"ID", u"Delim", u"Plus", u"Minus", 
                      u"Bar", u"LParen", u"RParen" ]

    RULE_geocards = 0
    RULE_body = 1
    RULE_region = 2
    RULE_zoneUnion = 3
    RULE_zone = 4
    RULE_expr = 5
    RULE_subZone = 6
    RULE_unaryExpression = 7
    RULE_geoDirective = 8
    RULE_expansion = 9
    RULE_translat = 10
    RULE_transform = 11
    RULE_lattice = 12

    ruleNames =  [ u"geocards", u"body", u"region", u"zoneUnion", u"zone", 
                   u"expr", u"subZone", u"unaryExpression", u"geoDirective", 
                   u"expansion", u"translat", u"transform", u"lattice" ]

    EOF = Token.EOF
    Whitespace=1
    InLineComment=2
    LineComment=3
    Newline=4
    End=5
    BodyCode=6
    Lattice=7
    RegionName=8
    StartExpansion=9
    StartTranslat=10
    StartTransform=11
    EndExpansion=12
    EndTranslat=13
    EndTransform=14
    Integer=15
    Float=16
    ID=17
    Delim=18
    Plus=19
    Minus=20
    Bar=21
    LParen=22
    RParen=23

    def __init__(self, input, output=sys.stdout):
        super(FlukaParser, self).__init__(input, output=output)
        self.checkVersion("4.7")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class GeocardsContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.GeocardsContext, self).__init__(parent, invokingState)
            self.parser = parser

        def body(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(FlukaParser.BodyContext)
            else:
                return self.getTypedRuleContext(FlukaParser.BodyContext,i)


        def region(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(FlukaParser.RegionContext)
            else:
                return self.getTypedRuleContext(FlukaParser.RegionContext,i)


        def lattice(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(FlukaParser.LatticeContext)
            else:
                return self.getTypedRuleContext(FlukaParser.LatticeContext,i)


        def getRuleIndex(self):
            return FlukaParser.RULE_geocards

        def enterRule(self, listener):
            if hasattr(listener, "enterGeocards"):
                listener.enterGeocards(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitGeocards"):
                listener.exitGeocards(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitGeocards"):
                return visitor.visitGeocards(self)
            else:
                return visitor.visitChildren(self)




    def geocards(self):

        localctx = FlukaParser.GeocardsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_geocards)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 29 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 29
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [FlukaParser.BodyCode, FlukaParser.StartExpansion, FlukaParser.StartTranslat, FlukaParser.StartTransform]:
                    self.state = 26
                    self.body()
                    pass
                elif token in [FlukaParser.RegionName]:
                    self.state = 27
                    self.region()
                    pass
                elif token in [FlukaParser.Lattice]:
                    self.state = 28
                    self.lattice()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 31 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << FlukaParser.BodyCode) | (1 << FlukaParser.Lattice) | (1 << FlukaParser.RegionName) | (1 << FlukaParser.StartExpansion) | (1 << FlukaParser.StartTranslat) | (1 << FlukaParser.StartTransform))) != 0)):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BodyContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.BodyContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return FlukaParser.RULE_body

     
        def copyFrom(self, ctx):
            super(FlukaParser.BodyContext, self).copyFrom(ctx)



    class BodyDefPunctDelimContext(BodyContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.BodyContext)
            super(FlukaParser.BodyDefPunctDelimContext, self).__init__(parser)
            self.copyFrom(ctx)

        def BodyCode(self):
            return self.getToken(FlukaParser.BodyCode, 0)
        def Delim(self, i=None):
            if i is None:
                return self.getTokens(FlukaParser.Delim)
            else:
                return self.getToken(FlukaParser.Delim, i)
        def ID(self, i=None):
            if i is None:
                return self.getTokens(FlukaParser.ID)
            else:
                return self.getToken(FlukaParser.ID, i)
        def Float(self, i=None):
            if i is None:
                return self.getTokens(FlukaParser.Float)
            else:
                return self.getToken(FlukaParser.Float, i)

        def enterRule(self, listener):
            if hasattr(listener, "enterBodyDefPunctDelim"):
                listener.enterBodyDefPunctDelim(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitBodyDefPunctDelim"):
                listener.exitBodyDefPunctDelim(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitBodyDefPunctDelim"):
                return visitor.visitBodyDefPunctDelim(self)
            else:
                return visitor.visitChildren(self)


    class BodyDefSpaceDelimContext(BodyContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.BodyContext)
            super(FlukaParser.BodyDefSpaceDelimContext, self).__init__(parser)
            self.copyFrom(ctx)

        def BodyCode(self):
            return self.getToken(FlukaParser.BodyCode, 0)
        def ID(self):
            return self.getToken(FlukaParser.ID, 0)
        def Integer(self):
            return self.getToken(FlukaParser.Integer, 0)
        def Float(self, i=None):
            if i is None:
                return self.getTokens(FlukaParser.Float)
            else:
                return self.getToken(FlukaParser.Float, i)

        def enterRule(self, listener):
            if hasattr(listener, "enterBodyDefSpaceDelim"):
                listener.enterBodyDefSpaceDelim(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitBodyDefSpaceDelim"):
                listener.exitBodyDefSpaceDelim(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitBodyDefSpaceDelim"):
                return visitor.visitBodyDefSpaceDelim(self)
            else:
                return visitor.visitChildren(self)


    class GeometryDirectiveContext(BodyContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.BodyContext)
            super(FlukaParser.GeometryDirectiveContext, self).__init__(parser)
            self.copyFrom(ctx)

        def geoDirective(self):
            return self.getTypedRuleContext(FlukaParser.GeoDirectiveContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterGeometryDirective"):
                listener.enterGeometryDirective(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitGeometryDirective"):
                listener.exitGeometryDirective(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitGeometryDirective"):
                return visitor.visitGeometryDirective(self)
            else:
                return visitor.visitChildren(self)



    def body(self):

        localctx = FlukaParser.BodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_body)
        self._la = 0 # Token type
        try:
            self.state = 51
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
            if la_ == 1:
                localctx = FlukaParser.GeometryDirectiveContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 33
                self.geoDirective()
                pass

            elif la_ == 2:
                localctx = FlukaParser.BodyDefSpaceDelimContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 34
                self.match(FlukaParser.BodyCode)
                self.state = 35
                _la = self._input.LA(1)
                if not(_la==FlukaParser.Integer or _la==FlukaParser.ID):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 37 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 36
                    self.match(FlukaParser.Float)
                    self.state = 39 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==FlukaParser.Float):
                        break

                pass

            elif la_ == 3:
                localctx = FlukaParser.BodyDefPunctDelimContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 41
                self.match(FlukaParser.BodyCode)
                self.state = 48
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==FlukaParser.Delim:
                    self.state = 42
                    self.match(FlukaParser.Delim)
                    self.state = 44
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if _la==FlukaParser.Float or _la==FlukaParser.ID:
                        self.state = 43
                        _la = self._input.LA(1)
                        if not(_la==FlukaParser.Float or _la==FlukaParser.ID):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()


                    self.state = 50
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class RegionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.RegionContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return FlukaParser.RULE_region

     
        def copyFrom(self, ctx):
            super(FlukaParser.RegionContext, self).copyFrom(ctx)



    class ComplexRegionContext(RegionContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.RegionContext)
            super(FlukaParser.ComplexRegionContext, self).__init__(parser)
            self.copyFrom(ctx)

        def RegionName(self):
            return self.getToken(FlukaParser.RegionName, 0)
        def Integer(self):
            return self.getToken(FlukaParser.Integer, 0)
        def zoneUnion(self):
            return self.getTypedRuleContext(FlukaParser.ZoneUnionContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterComplexRegion"):
                listener.enterComplexRegion(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitComplexRegion"):
                listener.exitComplexRegion(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitComplexRegion"):
                return visitor.visitComplexRegion(self)
            else:
                return visitor.visitChildren(self)


    class SimpleRegionContext(RegionContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.RegionContext)
            super(FlukaParser.SimpleRegionContext, self).__init__(parser)
            self.copyFrom(ctx)

        def RegionName(self):
            return self.getToken(FlukaParser.RegionName, 0)
        def Integer(self):
            return self.getToken(FlukaParser.Integer, 0)
        def zone(self):
            return self.getTypedRuleContext(FlukaParser.ZoneContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterSimpleRegion"):
                listener.enterSimpleRegion(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSimpleRegion"):
                listener.exitSimpleRegion(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitSimpleRegion"):
                return visitor.visitSimpleRegion(self)
            else:
                return visitor.visitChildren(self)



    def region(self):

        localctx = FlukaParser.RegionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_region)
        try:
            self.state = 59
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,6,self._ctx)
            if la_ == 1:
                localctx = FlukaParser.SimpleRegionContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 53
                self.match(FlukaParser.RegionName)
                self.state = 54
                self.match(FlukaParser.Integer)
                self.state = 55
                self.zone()
                pass

            elif la_ == 2:
                localctx = FlukaParser.ComplexRegionContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 56
                self.match(FlukaParser.RegionName)
                self.state = 57
                self.match(FlukaParser.Integer)
                self.state = 58
                self.zoneUnion()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ZoneUnionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.ZoneUnionContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return FlukaParser.RULE_zoneUnion

     
        def copyFrom(self, ctx):
            super(FlukaParser.ZoneUnionContext, self).copyFrom(ctx)



    class MultipleUnion2Context(ZoneUnionContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.ZoneUnionContext)
            super(FlukaParser.MultipleUnion2Context, self).__init__(parser)
            self.copyFrom(ctx)

        def zone(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(FlukaParser.ZoneContext)
            else:
                return self.getTypedRuleContext(FlukaParser.ZoneContext,i)

        def Bar(self, i=None):
            if i is None:
                return self.getTokens(FlukaParser.Bar)
            else:
                return self.getToken(FlukaParser.Bar, i)

        def enterRule(self, listener):
            if hasattr(listener, "enterMultipleUnion2"):
                listener.enterMultipleUnion2(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitMultipleUnion2"):
                listener.exitMultipleUnion2(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitMultipleUnion2"):
                return visitor.visitMultipleUnion2(self)
            else:
                return visitor.visitChildren(self)


    class MultipleUnionContext(ZoneUnionContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.ZoneUnionContext)
            super(FlukaParser.MultipleUnionContext, self).__init__(parser)
            self.copyFrom(ctx)

        def Bar(self, i=None):
            if i is None:
                return self.getTokens(FlukaParser.Bar)
            else:
                return self.getToken(FlukaParser.Bar, i)
        def zone(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(FlukaParser.ZoneContext)
            else:
                return self.getTypedRuleContext(FlukaParser.ZoneContext,i)


        def enterRule(self, listener):
            if hasattr(listener, "enterMultipleUnion"):
                listener.enterMultipleUnion(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitMultipleUnion"):
                listener.exitMultipleUnion(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitMultipleUnion"):
                return visitor.visitMultipleUnion(self)
            else:
                return visitor.visitChildren(self)


    class SingleUnionContext(ZoneUnionContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.ZoneUnionContext)
            super(FlukaParser.SingleUnionContext, self).__init__(parser)
            self.copyFrom(ctx)

        def Bar(self):
            return self.getToken(FlukaParser.Bar, 0)
        def zone(self):
            return self.getTypedRuleContext(FlukaParser.ZoneContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterSingleUnion"):
                listener.enterSingleUnion(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSingleUnion"):
                listener.exitSingleUnion(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitSingleUnion"):
                return visitor.visitSingleUnion(self)
            else:
                return visitor.visitChildren(self)



    def zoneUnion(self):

        localctx = FlukaParser.ZoneUnionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_zoneUnion)
        self._la = 0 # Token type
        try:
            self.state = 83
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,9,self._ctx)
            if la_ == 1:
                localctx = FlukaParser.MultipleUnionContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 61
                self.match(FlukaParser.Bar)
                self.state = 62
                self.zone()
                self.state = 65 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while True:
                    self.state = 63
                    self.match(FlukaParser.Bar)
                    self.state = 64
                    self.zone()
                    self.state = 67 
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if not (_la==FlukaParser.Bar):
                        break

                pass

            elif la_ == 2:
                localctx = FlukaParser.SingleUnionContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 69
                self.match(FlukaParser.Bar)
                self.state = 70
                self.zone()
                pass

            elif la_ == 3:
                localctx = FlukaParser.MultipleUnion2Context(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 71
                self.zone()
                self.state = 72
                self.match(FlukaParser.Bar)
                self.state = 78
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,8,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 73
                        self.zone()
                        self.state = 74
                        self.match(FlukaParser.Bar) 
                    self.state = 80
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,8,self._ctx)

                self.state = 81
                self.zone()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ZoneContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.ZoneContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expr(self):
            return self.getTypedRuleContext(FlukaParser.ExprContext,0)


        def subZone(self):
            return self.getTypedRuleContext(FlukaParser.SubZoneContext,0)


        def getRuleIndex(self):
            return FlukaParser.RULE_zone

        def enterRule(self, listener):
            if hasattr(listener, "enterZone"):
                listener.enterZone(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitZone"):
                listener.exitZone(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitZone"):
                return visitor.visitZone(self)
            else:
                return visitor.visitChildren(self)




    def zone(self):

        localctx = FlukaParser.ZoneContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_zone)
        try:
            self.state = 87
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,10,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 85
                self.expr()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 86
                self.subZone()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.ExprContext, self).__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return FlukaParser.RULE_expr

     
        def copyFrom(self, ctx):
            super(FlukaParser.ExprContext, self).copyFrom(ctx)



    class UnaryAndBooleanContext(ExprContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.ExprContext)
            super(FlukaParser.UnaryAndBooleanContext, self).__init__(parser)
            self.copyFrom(ctx)

        def unaryExpression(self):
            return self.getTypedRuleContext(FlukaParser.UnaryExpressionContext,0)

        def expr(self):
            return self.getTypedRuleContext(FlukaParser.ExprContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterUnaryAndBoolean"):
                listener.enterUnaryAndBoolean(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitUnaryAndBoolean"):
                listener.exitUnaryAndBoolean(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitUnaryAndBoolean"):
                return visitor.visitUnaryAndBoolean(self)
            else:
                return visitor.visitChildren(self)


    class OneSubZoneContext(ExprContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.ExprContext)
            super(FlukaParser.OneSubZoneContext, self).__init__(parser)
            self.copyFrom(ctx)

        def subZone(self):
            return self.getTypedRuleContext(FlukaParser.SubZoneContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterOneSubZone"):
                listener.enterOneSubZone(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitOneSubZone"):
                listener.exitOneSubZone(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitOneSubZone"):
                return visitor.visitOneSubZone(self)
            else:
                return visitor.visitChildren(self)


    class UnaryAndSubZoneContext(ExprContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.ExprContext)
            super(FlukaParser.UnaryAndSubZoneContext, self).__init__(parser)
            self.copyFrom(ctx)

        def subZone(self):
            return self.getTypedRuleContext(FlukaParser.SubZoneContext,0)

        def expr(self):
            return self.getTypedRuleContext(FlukaParser.ExprContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterUnaryAndSubZone"):
                listener.enterUnaryAndSubZone(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitUnaryAndSubZone"):
                listener.exitUnaryAndSubZone(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitUnaryAndSubZone"):
                return visitor.visitUnaryAndSubZone(self)
            else:
                return visitor.visitChildren(self)


    class SingleUnaryContext(ExprContext):

        def __init__(self, parser, ctx): # actually a FlukaParser.ExprContext)
            super(FlukaParser.SingleUnaryContext, self).__init__(parser)
            self.copyFrom(ctx)

        def unaryExpression(self):
            return self.getTypedRuleContext(FlukaParser.UnaryExpressionContext,0)


        def enterRule(self, listener):
            if hasattr(listener, "enterSingleUnary"):
                listener.enterSingleUnary(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSingleUnary"):
                listener.exitSingleUnary(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitSingleUnary"):
                return visitor.visitSingleUnary(self)
            else:
                return visitor.visitChildren(self)



    def expr(self):

        localctx = FlukaParser.ExprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_expr)
        try:
            self.state = 97
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
            if la_ == 1:
                localctx = FlukaParser.SingleUnaryContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 89
                self.unaryExpression()
                pass

            elif la_ == 2:
                localctx = FlukaParser.UnaryAndBooleanContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 90
                self.unaryExpression()
                self.state = 91
                self.expr()
                pass

            elif la_ == 3:
                localctx = FlukaParser.UnaryAndSubZoneContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 93
                self.subZone()
                self.state = 94
                self.expr()
                pass

            elif la_ == 4:
                localctx = FlukaParser.OneSubZoneContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 96
                self.subZone()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SubZoneContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.SubZoneContext, self).__init__(parent, invokingState)
            self.parser = parser

        def LParen(self):
            return self.getToken(FlukaParser.LParen, 0)

        def expr(self):
            return self.getTypedRuleContext(FlukaParser.ExprContext,0)


        def RParen(self):
            return self.getToken(FlukaParser.RParen, 0)

        def Minus(self):
            return self.getToken(FlukaParser.Minus, 0)

        def Plus(self):
            return self.getToken(FlukaParser.Plus, 0)

        def getRuleIndex(self):
            return FlukaParser.RULE_subZone

        def enterRule(self, listener):
            if hasattr(listener, "enterSubZone"):
                listener.enterSubZone(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSubZone"):
                listener.exitSubZone(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitSubZone"):
                return visitor.visitSubZone(self)
            else:
                return visitor.visitChildren(self)




    def subZone(self):

        localctx = FlukaParser.SubZoneContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_subZone)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 99
            _la = self._input.LA(1)
            if not(_la==FlukaParser.Plus or _la==FlukaParser.Minus):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 100
            self.match(FlukaParser.LParen)
            self.state = 101
            self.expr()
            self.state = 102
            self.match(FlukaParser.RParen)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class UnaryExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.UnaryExpressionContext, self).__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(FlukaParser.ID, 0)

        def Minus(self):
            return self.getToken(FlukaParser.Minus, 0)

        def Plus(self):
            return self.getToken(FlukaParser.Plus, 0)

        def getRuleIndex(self):
            return FlukaParser.RULE_unaryExpression

        def enterRule(self, listener):
            if hasattr(listener, "enterUnaryExpression"):
                listener.enterUnaryExpression(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitUnaryExpression"):
                listener.exitUnaryExpression(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitUnaryExpression"):
                return visitor.visitUnaryExpression(self)
            else:
                return visitor.visitChildren(self)




    def unaryExpression(self):

        localctx = FlukaParser.UnaryExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_unaryExpression)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 104
            _la = self._input.LA(1)
            if not(_la==FlukaParser.Plus or _la==FlukaParser.Minus):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 105
            self.match(FlukaParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class GeoDirectiveContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.GeoDirectiveContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expansion(self):
            return self.getTypedRuleContext(FlukaParser.ExpansionContext,0)


        def translat(self):
            return self.getTypedRuleContext(FlukaParser.TranslatContext,0)


        def transform(self):
            return self.getTypedRuleContext(FlukaParser.TransformContext,0)


        def getRuleIndex(self):
            return FlukaParser.RULE_geoDirective

        def enterRule(self, listener):
            if hasattr(listener, "enterGeoDirective"):
                listener.enterGeoDirective(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitGeoDirective"):
                listener.exitGeoDirective(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitGeoDirective"):
                return visitor.visitGeoDirective(self)
            else:
                return visitor.visitChildren(self)




    def geoDirective(self):

        localctx = FlukaParser.GeoDirectiveContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_geoDirective)
        try:
            self.state = 110
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [FlukaParser.StartExpansion]:
                self.enterOuterAlt(localctx, 1)
                self.state = 107
                self.expansion()
                pass
            elif token in [FlukaParser.StartTranslat]:
                self.enterOuterAlt(localctx, 2)
                self.state = 108
                self.translat()
                pass
            elif token in [FlukaParser.StartTransform]:
                self.enterOuterAlt(localctx, 3)
                self.state = 109
                self.transform()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpansionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.ExpansionContext, self).__init__(parent, invokingState)
            self.parser = parser

        def StartExpansion(self):
            return self.getToken(FlukaParser.StartExpansion, 0)

        def Float(self):
            return self.getToken(FlukaParser.Float, 0)

        def EndExpansion(self):
            return self.getToken(FlukaParser.EndExpansion, 0)

        def body(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(FlukaParser.BodyContext)
            else:
                return self.getTypedRuleContext(FlukaParser.BodyContext,i)


        def getRuleIndex(self):
            return FlukaParser.RULE_expansion

        def enterRule(self, listener):
            if hasattr(listener, "enterExpansion"):
                listener.enterExpansion(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitExpansion"):
                listener.exitExpansion(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitExpansion"):
                return visitor.visitExpansion(self)
            else:
                return visitor.visitChildren(self)




    def expansion(self):

        localctx = FlukaParser.ExpansionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_expansion)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 112
            self.match(FlukaParser.StartExpansion)
            self.state = 113
            self.match(FlukaParser.Float)
            self.state = 115 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 114
                self.body()
                self.state = 117 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << FlukaParser.BodyCode) | (1 << FlukaParser.StartExpansion) | (1 << FlukaParser.StartTranslat) | (1 << FlukaParser.StartTransform))) != 0)):
                    break

            self.state = 119
            self.match(FlukaParser.EndExpansion)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TranslatContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.TranslatContext, self).__init__(parent, invokingState)
            self.parser = parser

        def StartTranslat(self):
            return self.getToken(FlukaParser.StartTranslat, 0)

        def Float(self, i=None):
            if i is None:
                return self.getTokens(FlukaParser.Float)
            else:
                return self.getToken(FlukaParser.Float, i)

        def EndTranslat(self):
            return self.getToken(FlukaParser.EndTranslat, 0)

        def body(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(FlukaParser.BodyContext)
            else:
                return self.getTypedRuleContext(FlukaParser.BodyContext,i)


        def getRuleIndex(self):
            return FlukaParser.RULE_translat

        def enterRule(self, listener):
            if hasattr(listener, "enterTranslat"):
                listener.enterTranslat(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitTranslat"):
                listener.exitTranslat(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitTranslat"):
                return visitor.visitTranslat(self)
            else:
                return visitor.visitChildren(self)




    def translat(self):

        localctx = FlukaParser.TranslatContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_translat)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 121
            self.match(FlukaParser.StartTranslat)
            self.state = 122
            self.match(FlukaParser.Float)
            self.state = 123
            self.match(FlukaParser.Float)
            self.state = 124
            self.match(FlukaParser.Float)
            self.state = 126 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 125
                self.body()
                self.state = 128 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << FlukaParser.BodyCode) | (1 << FlukaParser.StartExpansion) | (1 << FlukaParser.StartTranslat) | (1 << FlukaParser.StartTransform))) != 0)):
                    break

            self.state = 130
            self.match(FlukaParser.EndTranslat)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class TransformContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.TransformContext, self).__init__(parent, invokingState)
            self.parser = parser

        def StartTransform(self):
            return self.getToken(FlukaParser.StartTransform, 0)

        def EndTransform(self):
            return self.getToken(FlukaParser.EndTransform, 0)

        def ID(self):
            return self.getToken(FlukaParser.ID, 0)

        def Integer(self):
            return self.getToken(FlukaParser.Integer, 0)

        def body(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(FlukaParser.BodyContext)
            else:
                return self.getTypedRuleContext(FlukaParser.BodyContext,i)


        def getRuleIndex(self):
            return FlukaParser.RULE_transform

        def enterRule(self, listener):
            if hasattr(listener, "enterTransform"):
                listener.enterTransform(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitTransform"):
                listener.exitTransform(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitTransform"):
                return visitor.visitTransform(self)
            else:
                return visitor.visitChildren(self)




    def transform(self):

        localctx = FlukaParser.TransformContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_transform)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 132
            self.match(FlukaParser.StartTransform)
            self.state = 133
            _la = self._input.LA(1)
            if not(_la==FlukaParser.Integer or _la==FlukaParser.ID):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 135 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 134
                self.body()
                self.state = 137 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << FlukaParser.BodyCode) | (1 << FlukaParser.StartExpansion) | (1 << FlukaParser.StartTranslat) | (1 << FlukaParser.StartTransform))) != 0)):
                    break

            self.state = 139
            self.match(FlukaParser.EndTransform)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class LatticeContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(FlukaParser.LatticeContext, self).__init__(parent, invokingState)
            self.parser = parser

        def Lattice(self):
            return self.getToken(FlukaParser.Lattice, 0)

        def ID(self, i=None):
            if i is None:
                return self.getTokens(FlukaParser.ID)
            else:
                return self.getToken(FlukaParser.ID, i)

        def getRuleIndex(self):
            return FlukaParser.RULE_lattice

        def enterRule(self, listener):
            if hasattr(listener, "enterLattice"):
                listener.enterLattice(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitLattice"):
                listener.exitLattice(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitLattice"):
                return visitor.visitLattice(self)
            else:
                return visitor.visitChildren(self)




    def lattice(self):

        localctx = FlukaParser.LatticeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_lattice)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 141
            self.match(FlukaParser.Lattice)
            self.state = 143 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 142
                self.match(FlukaParser.ID)
                self.state = 145 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==FlukaParser.ID):
                    break

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





