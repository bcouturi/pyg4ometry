==========
Authorship
==========

The following people have contributed to pyg4ometry:

* Stewart Boogert
* Andrey Abramov
* Alistair Butcher
* Laurie Nevay
* William Shields
* Stuart Walker
* Ben Shellswell

