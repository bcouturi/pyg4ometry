pyg4ometry Documentation
========================

pyg4ometry is a package to create, load, write and visualise solid geometry for particle tracking simulations. 

.. toctree::
   :maxdepth: 3

   licence
   authorship
   installation
   introduction
   pythonscripting
   tutorials
   tutorialsAdvanced
   moduledocs
   developer
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
